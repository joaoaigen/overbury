@extends('layouts.app')

<?php
$storage = new \App\Storage();
$products = new \App\Products();
?>
@section('title-head')
    Storage
@endsection

@section('title-body')
    Storage
@endsection

@section('page-css')

    <style type="text/css">
        ::-webkit-input-placeholder {
            color: white;
        }

        :-moz-placeholder { /* Firefox 18- */
            color: white;
        }

        ::-moz-placeholder { /* Firefox 19+ */
            color: white;
        }

        :-ms-input-placeholder {
            color: white;
        }
    </style>

@endsection
@section('main-content')
    <section class="content">

        <div class="row">
            <div class="col-xl-12 col-md-12 col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">New item </h3>
                    </div>
                    <div class="box-body">
                        <form method="post" action="{{ route('storage.store') }}">
                        {{ csrf_field() }}
                        <!-- Date dd/mm/yyyy -->

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-6">
                                        <label>Product</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-archive" aria-hidden="true"></i>
                                                </div>
                                            </div>

                                            <select type="text" class="form-control" id="id_product" required="required" name="id_product">
                                                <option>Select an option</option>
                                                @foreach($products->allData() as $row)
                                                    @if(\App\Storage::where('id_product', '=', $row->id_products)->first())
                                                    @else
                                                        <option value="{{ $row->id_products }}"> {{ $row->products_name }}</option>
                                                    @endif

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-6">
                                        <label>Current quantity</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fa fa-balance-scale" aria-hidden="true"></i>
                                                </div>
                                            </div>

                                            <input type="text" class="form-control" id="quantity_current"
                                                   required="required"
                                                   placeholder="Insert a quantity" name="quantity_current">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Insert</button>
                            </div>
                        </form>
                        <!-- /.form group -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-12">
                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Storage List</h3>
                        <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example"
                                   class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                <tr class="text-center">
                                    <th>NAME</th>
                                    <th>CURRENT QUANTITY</th>
                                    <th>OPTIONS</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                @foreach($storage->allData() as $row)
                                    <tr>
                                        <td>{{ $row->products_name }}</td>
                                        <td class="font-size-30 text-center sorting">{{ $row->quantity_current }}</td>
                                        <td>
                                            <button class="btn btn-sm btn-success" type="button"
                                                    onclick="$('#modalUpdateStorage').modal('show'); showStorage({{ $row->id_storage }})"
                                                    value="{{ $row->id_storage }}">Update
                                            </button><br>
                                            <form method="get" action="{{ route('storage.delete') }}">
                                                <input hidden name="id_storage" value="{{ $row->id_storage}}">
                                                <button class="btn btn-sm btn-danger" type="submit">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="text-center">
                                    <th>NAME</th>
                                    <th>CURRENT QUANTITY</th>
                                    <th>OPTIONS</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>

    </section>

    <div class="modal modal-right fade" id="modalUpdateStorage" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update storage</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6>
                        In this session you can update the current quantity of products in storage: <b
                                id="productNameStorage"></b>
                    </h6>
                    <form id="updateStorage" method="post" action="{{ route('storage.update') }}">
                        @csrf
                        <input hidden name="id_storage" id="id_storage" value="">
                        <div class="form-group">
                            <label>Product name</label>
                            <input class="form-control" type="text" id="products_name" name="products_name"
                                   value="">
                        </div>

                        <div class="form-group">
                            <label>Current quantity</label>
                            <input class="form-control valor" type="text" readonly name="quantity_current" step="any"
                                   id="modal_quantity_current">
                        </div>

                        <div class="form-group">
                            <label>Quantity take off</label>
                            <input class="form-control valor" type="text" onkeyup="quantity(this.value);console.log(this.value);" name="quantity_takeoff" step="any"
                                   id="quantity_takeoff">
                        </div>

                        <div class="form-group">
                            <label>Alert quantity</label>
                            <input class="form-control valor" type="text" required name="alert_quantity" step="any"
                                   id="modal_alert_quantity">
                        </div>
                    </form>
                </div>
                <div class="modal-footer modal-footer-uniform">
                    <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close modal
                    </button>
                    <button type="submit" class="btn btn-bold btn-pure btn-primary float-right"
                            onclick="$('#updateStorage').submit()">Update
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('page-js')

    <!-- InputMask -->
    <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- date-range-picker -->
    <script src="{{ asset('assets/vendor_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- bootstrap datepicker -->
    <script src="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <!-- bootstrap color picker -->
    <script src="{{ asset('assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>

    <!-- bootstrap time picker -->
    <script src="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <!-- SlimScroll -->
    <script src="{{ asset('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/pages/advanced-form-element.js') }}"></script>

    <!-- FastClick -->
    <script src="{{ asset('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- This is data table -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/select2/dist/js/select2.js') }}x"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('js/pages/data-table.js') }}"></script>

    <script src="{{ asset('js/jquery.mask.js') }}"></script>
    <script src="{{ asset('js/jquery.easy-autocomplete.min.js') }}"></script>
    <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script type="text/javascript">

        $('#mesas').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [[8, "asc"]]
        });

    </script>

    <script type="text/javascript">
        var quantity_current = 0;

        function showStorage(id_storage) {
            $('#id_storage').val(id_storage);

            $.ajax({
                type: 'GET',
                url: '{{ route('storage.show') }}',
                data: {'id_storage': id_storage, 'req_ajax': 1},
                success: function (data) {
                    console.log(data);
                    quantity_current = data.quantity_current;
                    $('#modal_quantity_current').val(data.quantity_current);
                    $('#modal_alert_quantity').val(data.alert_quantity);
                    $('#products_name').val(data.products_name);
                    $('#productNameStorage').val(data.products_name);
                },
                error: function (data) {
                }
            });
        }

        function quantity(takeoff){
            if(parseInt(takeoff) > 0){
                $('#modal_quantity_current').empty();
                $('#modal_quantity_current').val(parseInt(quantity_current) - parseInt(takeoff));
            }else{
                $('#modal_quantity_current').val(quantity_current);
            }

        }

        $(document).ready(function () {

            $("#usuarios").easyAutocomplete(options);


            @if(session()->has('status') && session()->get('status') == 200)
            swal("Sucesso!", "{{ session()->get('msg') }}", "success");
            @endif
            @if(session()->has('status') && session()->get('status') == 400)
            swal("Erro!", "{{ session()->get('msg') }}", "error");
            @endif

            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.num').mask('0000000000');
            $('.celular').mask('(00) 0 0000-0000');
            $('.telefone').mask('(00) 0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0.00", {reverse: true});
            $('.quantidade').mask("#.##0.000", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
    </script>
@endsection
