@extends('layouts.app')

@section('title-head')
    Companies
@endsection

@section('title-body')
    Companies
@endsection

@section('page-css')

    <style type="text/css">
        ::-webkit-input-placeholder {
            color: white;
        }

        :-moz-placeholder { /* Firefox 18- */
            color: white;
        }

        ::-moz-placeholder { /* Firefox 19+ */
            color: white;
        }

        :-ms-input-placeholder {
            color: white;
        }
    </style>

@endsection
@section('main-content')
    <section class="content">

        <div class="row">
            <div class="col-12">
                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Companies List</h3>
                        <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example"
                                   class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                <tr class="text-center">
                                    <th>NAME</th>
                                    <th>NICKNAME</th>
                                    <th>DATABASE</th>
                                    <th>EMAIL</th>
                                    <th>OPTIONS</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                @foreach($companie as $row)
                                    <tr>
                                        <td>{{ $row->companie_name }}</td>
                                        <td>{{ $row->companie_nickname }}</td>
                                        <td>{{ $row->companie_database }}</td>
                                        <td>{{ $row->companie_email }}</td>
                                        <td>
                                            <button class="btn btn-sm btn-success" type="button"
                                                    onclick="$('#modalUpdateCompanies').modal('show'); showCompanies({{ $row->id_companie }})"
                                                    value="{{ $row->id_companie }}">Update
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="text-center">
                                    <th>NAME</th>
                                    <th>NICKNAME</th>
                                    <th>DATABASE</th>
                                    <th>EMAIL</th>
                                    <th>OPTIONS</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>

    </section>

    <div class="modal modal-right fade" id="modalUpdateCompanies" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update companie</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6>
                        In this session you can update the name of companies.
                    </h6>
                    <form id="updateCompanies" method="post" action="{{ route('companie.update') }}">
                        @csrf
                        <input hidden name="id_companie" id="id_companie" value="">
                        <div class="form-group">
                            <label>Companie database</label>
                            <input class="form-control" type="text" id="modal_companie_database" name="companie_database">
                        </div>
                    </form>
                </div>
                <div class="modal-footer modal-footer-uniform">
                    <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close modal
                    </button>
                    <button type="submit" class="btn btn-bold btn-pure btn-primary float-right"
                            onclick="$('#updateCompanies').submit()">Update
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('page-js')

    <!-- InputMask -->
    <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- date-range-picker -->
    <script src="{{ asset('assets/vendor_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- bootstrap datepicker -->
    <script src="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <!-- bootstrap color picker -->
    <script src="{{ asset('assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>

    <!-- bootstrap time picker -->
    <script src="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <!-- SlimScroll -->
    <script src="{{ asset('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/pages/advanced-form-element.js') }}"></script>

    <!-- FastClick -->
    <script src="{{ asset('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- This is data table -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/select2/dist/js/select2.js') }}x"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('js/pages/data-table.js') }}"></script>

    <script src="{{ asset('js/jquery.mask.js') }}"></script>
    <script src="{{ asset('js/jquery.easy-autocomplete.min.js') }}"></script>
    <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script type="text/javascript">

        $('#mesas').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [[8, "asc"]]
        });

    </script>

    <script type="text/javascript">
        function showCompanies(id_companie) {
            $('#id_companie').val(id_companie);

            $.ajax({
                type: 'GET',
                url: '{{ route('companie.show') }}',
                data: {'id_companie': id_companie, 'req_ajax': 1},
                success: function (data) {
                    $('#modal_companie_database').val(data.companie_database);
                },
                error: function (data) {
                }
            });
        }

        $(document).ready(function () {

            $("#usuarios").easyAutocomplete(options);


            @if(session()->has('status') && session()->get('status') == 200)
            swal("Sucesso!", "{{ session()->get('msg') }}", "success");
            @endif
            @if(session()->has('status') && session()->get('status') == 400)
            swal("Erro!", "{{ session()->get('msg') }}", "error");
            @endif

            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00000-000');
            $('.num').mask('0000000000');
            $('.celular').mask('(00) 0 0000-0000');
            $('.telefone').mask('(00) 0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0.00", {reverse: true});
            $('.quantidade').mask("#.##0.000", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                    'Z': {
                        pattern: /[0-9]/, optional: true
                    }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                        pattern: /[\/]/,
                        fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
            });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
        });
    </script>
@endsection
