<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="public/images/favicon.ico">

        <title>Overbury - Sign Up </title>

        <!-- Bootstrap 4.0-->
        <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">

        <!-- Bootstrap extend-->
        <link rel="stylesheet" href="css/bootstrap-extend.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="css/master_style.css">

        <!-- Crypto_Admin skins -->
        <link rel="stylesheet" href="css/skins/_all-skins.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="hold-transition register-page">
        <div class="register-box">

            <div class="register-box-body">
                <div class="register-logo">
                    <a href="{{ route('login') }}"><b>Overbury</b></a>
                </div>
                <p class="login-box-msg">Register a new user</p>

                @if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Ops!</strong> We got a problem. Try again in few minutes<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="{{ route('register') }}" class="form-element">
                    @csrf

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required name="companie_name" value="{{ old('companie_name') }}" placeholder="Companie name">
                        <span class="ion ion-person form-control-feedback "></span>
                        @error('companie_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required name="companie_nickname" value="{{ old('companie_nickname') }}" placeholder="Companie nickname">
                        <span class="ion ion-person form-control-feedback "></span>
                        @error('companie_nickname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" required name="companie_email" value="{{ old('companie_email') }}" placeholder="Companie e-mail">
                        <span class="ion ion-person form-control-feedback "></span>
                        @error('companie_email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <hr>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required name="first_name" value="{{ old('first_name') }}" placeholder="First Name">
                        <span class="ion ion-person form-control-feedback "></span>
                        @error('first_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required name="last_name" value="{{ old('last_name') }}" placeholder="Last name">
                        <span class="ion ion-person form-control-feedback "></span>
                        @error('last_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control" required value="{{ old('email') }}" placeholder="E-mail">
                        <span class="ion ion-email form-control-feedback "></span>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" required  placeholder="Password">
                        <span class="ion ion-locked form-control-feedback "></span>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password_confirmation" class="form-control" required placeholder="Password Confirmation">
                        <span class="ion ion-log-in form-control-feedback "></span>
                    </div>
                    <div class="row">
                        <!-- <div class="col-12">
                            <div class="checkbox">
                                <input type="checkbox" id="basic_checkbox_1" >
                                <label for="basic_checkbox_1">Eu aceito os <a href="#"><b>Termos</b></a></label>
                            </div>
                        </div>
                        /.col -->
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-info btn-block margin-top-10">Sign Up</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                {{-- <div class="social-auth-links text-center">
                    <p>- OU -</p>
                    <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
                </div> --}}
                <!-- /.social-auth-links -->

                <div class="margin-top-20 text-center">
                    <p>Já tem uma conta?<a href="{{ route('login') }}" class="text-info m-l-5"> Sign in</a></p>
                </div>

            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->


        <!-- jQuery 3 -->
        <script src="{{ asset('assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

        <!-- popper -->
        <script src="{{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>

        <!-- Bootstrap 4.0-->
        <script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>


    </body>
</html>
