<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('css/img/NSystemSolutions.png') }}">

    <title>Build Management- Lockscreen </title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-extend.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/master_style.css') }}">

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">

    <div class="lockscreen-box-body">

        <div class="lockscreen-logo">
            <a href="{{ url('/lockscreen') }}"><b>Building</b>Management</a>
        </div>

        <!-- START LOCK SCREEN ITEM -->
        <div class="lockscreen-item">
            <!-- lockscreen image -->
            <div class="lockscreen-image">
                <img src="{{ asset('images/user-info.jpg') }}" alt="User Image">
            </div>
            <!-- /.lockscreen-image -->

            <!-- User name -->
            <div class="lockscreen-name margin-top-20"><h3>
                    Hello {{ \Illuminate\Support\Facades\Auth::user()->first_name }}, welcome!</h3></div>
            <div class="">
                <h6>For now you can send invitations so that your employees can also create a user account just fill in
                    an email below that an invitation will be sent. </h6>
            </div>
            <!-- lockscreen credentials (contains the form) -->
            <form class="lockscreen-credentials form-element margin-top-20" method="post"
                  action="{{ route('send-invitation') }}">
                @csrf
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" name="email" placeholder="E-mail for invitation...">
                    <span class="ion ion-locked form-control-feedback "></span>
                </div>
                <!-- /.col -->
                <div class="text-center">
                    <button type="submit" class="btn btn-info btn-block margin-top-10">Invite</button>
                </div>
                <!-- /.col -->
            </form>
            <!-- /.lockscreen credentials -->

        </div>
        <!-- /.lockscreen-item -->
        <div class="help-block text-center margin-top-20">
            We are creating your database, soon the system will be released and you will be able to enjoy all the
            functions :)
        </div>
        <div class="text-center">
            <a href="{{ url('/logout') }}" class="link" data-toggle="tooltip" title="@lang('messages.logout')"
               data-original-title="Logout"><h3><b><i>Logout</i></b></h3></a>
        </div>
    </div>
</div>
<!-- /.center -->

<!-- jQuery 3 -->
<script src="{{ asset('assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

<!-- popper -->
<script src=" {{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>

<!-- Bootstrap 4.0-->
<script src=" {{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        @if(session()->has('status') && session()->get('status') == 200)
        swal("Sucesso!", "{{ session()->get('msg') }}", "success");
        @endif
        @if(session()->has('status') && session()->get('status') == 400)
        swal("Erro!", "{{ session()->get('msg') }}", "error");
        @endif
    });
</script>
</body>
</html>
