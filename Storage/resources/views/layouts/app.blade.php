<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('css/img/NSystemSolutions.png') }}">

    <title>Overbury - @yield('title-head')</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-extend.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/master_style.css') }}">

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css') }}">

    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendor_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('font-awesome-4.6.3/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('ionicons-2.0.1/css/ionicons.css') }}">
    <!--amcharts -->
    <link href="https://www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css"/>


    <script src="{{ url('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') }}"></script>
    <script src="{{ url('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>

    @yield('page-css')

    <style>
        .example-modal .modal {
            position: relative;
            top: auto;
            bottom: auto;
            right: auto;
            left: auto;
            display: block;
            z-index: 1;
        }

        .example-modal .modal {
            background: transparent !important;
        }

        .fontawesome-icon-list .fa-hover, .ion-icon-list .ion-hover {
            cursor: pointer;
            line-height: 50px;
            white-space: nowrap;
            color: #adadad;
            font-weight: 300;
            font-size: 16px;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .fontawesome-icon-list .fa-hover i, .ion-icon-list .ion-hover i {
            padding-right: 10px;
            font-size: 18px;
        }

        .fontawesome-icon-list .fa-hover:hover i, .ion-icon-list .ion-hover:hover i {
            color: #3aa0dc;
        }

        .fontawesome-icon-list .fa-hover:hover, .ion-icon-list .ion-hover:hover {
            background-color: #f1f1f1;
            color: #111;
        }

        /* FROM HTTP://WWW.GETBOOTSTRAP.COM
         * Glyphicons
         *
         * Special styles for displaying the icons and their classes in the docs.
         */

        .bs-glyphicons {
            padding-left: 0;
            padding-bottom: 1px;
            margin-bottom: 20px;
            list-style: none;
            overflow: hidden;
        }

        .bs-glyphicons li {
            float: left;
            width: 25%;
            height: 115px;
            padding: 10px;
            margin: 0 -1px -1px 0;
            font-size: 12px;
            line-height: 1.4;
            text-align: center;
            border: 1px solid #585858;
            color: #adadad;
        }

        .bs-glyphicons .glyphicon {
            margin-top: 5px;
            margin-bottom: 10px;
            font-size: 24px;
        }

        .bs-glyphicons .glyphicon-class {
            display: block;
            text-align: center;
            word-wrap: break-word; /* Help out IE10+ with class names */
        }

        .bs-glyphicons li:hover {
            background-color: #f1f1f1;
            color: #111;
        }

        .bs-glyphicons li:hover .glyphicon {
            color: #3aa0dc;
        }

        @media (min-width: 992px) {
            .bs-glyphicons li {
                width: 12.5%;
            }
        }

        @media (max-width: 767px) {
            .bs-glyphicons li {
                width: 50%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="index.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <b class="logo-mini">
                <span class="light-logo"><img src="../images/logo_overburypng" alt="logo"></span>
                <span class="dark-logo"><img src="../images/logo_overbury.png" alt="logo"></span>
            </b>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
		  <img src="../images/logo_overbury.png" alt="logo" class="light-logo">
	  	  <img src="../images/logo_overbury.png" alt="logo" class="dark-logo">
	  </span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    {{--<li class="search-box">
                        <a class="nav-link hidden-sm-down" href=" javascript:void(0)"><i class="mdi mdi-magnify"></i></a>
                        <form class="app-search" style="display: none;">
                            <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                        </form>
                    </li>

                    <!-- Messages -->
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="mdi mdi-email"></i>
                        </a>
                        <ul class="dropdown-menu scale-up">
                            <li class="header">You have 5 messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu inner-content-div">
                                    <li><!-- start message -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="../images/user2-160x160.jpg" class="rounded-circle" alt="User Image">
                                            </div>
                                            <div class="mail-contnet">
                                                <h4>
                                                    Lorem Ipsum
                                                    <small><i class="fa fa-clock-o"></i> 15 mins</small>
                                                </h4>
                                                <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end message -->
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="../images/user3-128x128.jpg" class="rounded-circle" alt="User Image">
                                            </div>
                                            <div class="mail-contnet">
                                                <h4>
                                                    Nullam tempor
                                                    <small><i class="fa fa-clock-o"></i> 4 hours</small>
                                                </h4>
                                                <span>Curabitur facilisis erat quis metus congue viverra.</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="../images/user4-128x128.jpg" class="rounded-circle" alt="User Image">
                                            </div>
                                            <div class="mail-contnet">
                                                <h4>
                                                    Proin venenatis
                                                    <small><i class="fa fa-clock-o"></i> Today</small>
                                                </h4>
                                                <span>Vestibulum nec ligula nec quam sodales rutrum sed luctus.</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="../images/user3-128x128.jpg" class="rounded-circle" alt="User Image">
                                            </div>
                                            <div class="mail-contnet">
                                                <h4>
                                                    Praesent suscipit
                                                    <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                </h4>
                                                <span>Curabitur quis risus aliquet, luctus arcu nec, venenatis neque.</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="../images/user4-128x128.jpg" class="rounded-circle" alt="User Image">
                                            </div>
                                            <div class="mail-contnet">
                                                <h4>
                                                    Donec tempor
                                                    <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                </h4>
                                                <span>Praesent vitae tellus eget nibh lacinia pretium.</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">See all e-Mails</a></li>
                        </ul>
                    </li>
                    <!-- Notifications -->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="mdi mdi-bell"></i>
                        </a>
                        <ul class="dropdown-menu scale-up">
                            <li class="header">You have 7 notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu inner-content-div">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> Curabitur id eros quis nunc suscipit blandit.
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-warning text-yellow"></i> Duis malesuada justo eu sapien elementum, in semper diam posuere.
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-red"></i> Donec at nisi sit amet tortor commodo porttitor pretium a erat.
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-shopping-cart text-green"></i> In gravida mauris et nisi
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> Praesent eu lacus in libero dictum fermentum.
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> Nunc fringilla lorem
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> Nullam euismod dolor ut quam interdum, at scelerisque ipsum imperdiet.
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- Tasks -->
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="mdi mdi-message"></i>
                        </a>
                        <ul class="dropdown-menu scale-up">
                            <li class="header">You have 6 tasks</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu inner-content-div">
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Lorem ipsum dolor sit amet
                                                <small class="pull-right">30%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-aqua" style="width: 30%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">30% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Vestibulum nec ligula
                                                <small class="pull-right">20%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-danger" style="width: 20%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">20% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Donec id leo ut ipsum
                                                <small class="pull-right">70%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-light-blue" style="width: 70%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">70% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Praesent vitae tellus
                                                <small class="pull-right">40%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-yellow" style="width: 40%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">40% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Nam varius sapien
                                                <small class="pull-right">80%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-red" style="width: 80%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">80% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Nunc fringilla
                                                <small class="pull-right">90%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-primary" style="width: 90%" role="progressbar"
                                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">90% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">View all tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../images/user5-128x128.jpg" class="user-image rounded-circle" alt="User Image">
                        </a>
                        <ul class="dropdown-menu scale-up">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="../images/user5-128x128.jpg" class="float-left rounded-circle" alt="User Image">

                                <p>
                                    Romi Roy
                                    <small class="mb-5">jb@gmail.com</small>
                                    <a href="#" class="btn btn-danger btn-sm btn-rounded">View Profile</a>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row no-gutters">
                                    <div class="col-12 text-left">
                                        <a href="#"><i class="ion ion-person"></i> My Profile</a>
                                    </div>
                                    <div class="col-12 text-left">
                                        <a href="#"><i class="ion ion-email-unread"></i> Inbox</a>
                                    </div>
                                    <div class="col-12 text-left">
                                        <a href="#"><i class="ion ion-settings"></i> Setting</a>
                                    </div>
                                    <div role="separator" class="divider col-12"></div>
                                    <div class="col-12 text-left">
                                        <a href="#"><i class="ti-settings"></i> Account Setting</a>
                                    </div>
                                    <div role="separator" class="divider col-12"></div>
                                    <div class="col-12 text-left">
                                        <a href="#"><i class="fa fa-power-off"></i> Logout</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-cog fa-spin"></i></a>
                    </li>--}}
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <!-- sidebar -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="ulogo">
                    <a href="{{ url('/home') }}">
                        <!-- logo for regular state and mobile devices -->

                        <span><b>Overbury </b></span>
                    </a>
                </div>
                <div class="image">
                    <img style="width: 300px; height: 100px" src="{{ asset('images/user1-128x128.jpg') }}"
                         class="rounded-circle" alt="User Image">
                </div>
                <div class="info">
                    <a href="{{ url('painel/meus-dados') }}" class="link" data-toggle="tooltip" title="Perfil"
                       data-original-title="Settings"><i class="fa-spin ion ion-gear-b"></i></a>
                    {{--<a href="" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i class="ion ion-android-mail"></i></a>--}}
                    <a href="{{ url('/logout') }}" class="link" data-toggle="tooltip" title="@lang('messages.logout')"
                       data-original-title="Logout"><i class="ion ion-power"></i></a>
                </div>
            </div>
            <!-- sidebar menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="nav-devider"></li>
                <li class="nav-devider"></li>
                <li class="header nav-small-cap">Account</li>
                <li class="{{ Request::segment(2) == 'home' ? 'active' : '' }}">
                    <a href="{{ url('/home') }}">
                        <i class="fa fa-home"></i> <span>Home</span>
                        <span class="pull-right-container">
                            {{--<i class="fa fa-angle-right pull-right"></i>--}}
                        </span>
                    </a>
                </li>
                {{-- <li class="{{ Request::segment(2) == 'perfil' ? 'active' : '' }}">
                     <a href="{{ url('usuario/perfil') }}">
                         <i class="fa fa-user"></i> <span>Perfil</span>
                         <span class="pull-right-container">
                             <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                </li>--}}
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i>
                        <span>Register</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('storage')  }}">Storage</a></li>
                        <li><a href="{{ route('products')  }}">Products</a></li>
                        <li><a href="{{ route('category')  }}">Category</a></li>
                    </ul>
                </li>

                @if(\Illuminate\Support\Facades\Auth::user())
                    @if(\Illuminate\Support\Facades\Auth::user()->nivel_master == 'Developer' or \Illuminate\Support\Facades\Auth::user()->nivel_master == 'Admin')
                        <br>
                        <li class="header nav-small-cap">Administrator</li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i>
                                <span>Users</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('products')  }}">Products</a></li>
                                <li><a href="{{ route('category')  }}">Category</a></li>
                            </ul>
                        </li>
                        @if(\Illuminate\Support\Facades\Auth::user()->nivel_master == 'Developer')
                            <br>
                            <li class="header nav-small-cap">Developer</li>

                            <li class="{{ Request::segment(2) == 'companie' ? 'active' : '' }}">
                                <a href="{{ url('/companie') }}">
                                    <i class="fa fa-building"></i> <span>Companie</span>
                                    <span class="pull-right-container">
                            {{--<i class="fa fa-angle-right pull-right"></i>--}}
                        </span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('/routes') }}">
                                    <i class="fa fa-building"></i> <span>Routes</span>
                                    <span class="pull-right-container">
                            {{--<i class="fa fa-angle-right pull-right"></i>--}}
                        </span>
                                </a>
                            </li>
                        @endif
                    @endif
                @endif
                <li>
                    <a href="{{ url('/logout') }}">
                        <i class="fa fa-sign-out"></i> <span>Sair</span>
                        <span class="pull-right-container">
                            {{--<i class="fa fa-angle-right pull-right"></i>--}}
                        </span>
                    </a>
                </li>
            </ul>
        </section>
    </aside>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                @yield('title-body')
                <small>@yield('title-body-descript')</small>
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('painel/home') }}"><i class="fa fa-dashboard"></i> Home</a>
                </li>
                <li class="breadcrumb-item active">@yield('title-head')</li>
            </ol>
        </section>
        @yield('main-content')
    </div>

    <footer class="main-footer">
        &copy; 2019 <a href="{{env('SITE_URL')}}">Overbury</a>
    </footer>
</div>

<script src="{{ asset("assets/vendor_components/jquery/dist/jquery.min.js") }}"></script>

<!-- popper -->
<!-- <script src="{{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script> -->

<!-- Bootstrap 4.0-->
<script src="{{ asset("assets/vendor_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>

<script src="{{ asset("assets/vendor_components/select2/dist/js/select2.full.js") }}"></script>
<!-- SlimScroll -->
<script src="{{ asset("assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js") }}"></script>

<!-- FastClick -->
<script src="{{ asset("assets/vendor_components/fastclick/lib/fastclick.js") }}"></script>

<!-- Crypto_Admin App -->
<script src="{{ asset("js/template.js") }}"></script>

<!-- Crypto_Admin for demo purposes -->
<script src="{{ asset("js/demo.js") }}"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset("assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js") }}"></script>

<!--amcharts charts -->
<script src="{{ url("https://www.amcharts.com/lib/3/amcharts.js") }}"></script>
<script src="{{ url("https://www.amcharts.com/lib/3/serial.js") }}"></script>
<script src="{{ url("https://www.amcharts.com/lib/3/amstock.js") }}"></script>
<script src="{{ url("https://www.amcharts.com/lib/3/plugins/export/export.min.js") }}"></script>
<script src="{{ url("https://www.amcharts.com/lib/3/themes/black.js") }}"></script>

<!-- EChartJS JavaScript -->
<script src="{{ asset("assets/vendor_components/echarts-master/dist/echarts-en.min.js") }}"></script>
<script src="{{ asset("assets/vendor_components/echarts-liquidfill-master/dist/echarts-liquidfill.min.js") }}"></script>

<!-- webticker -->
<script src="{{ asset("assets/vendor_components/Web-Ticker-master/jquery.webticker.min.js") }}"></script>

<!-- EChartJS JavaScript -->
<script src="{{ asset("assets/vendor_components/echarts-master/dist/echarts-en.min.js") }}"></script>
<script src="{{ asset("assets/vendor_components/echarts-liquidfill-master/dist/echarts-liquidfill.min.js") }}"></script>

<!-- This is data table -->
<script src="{{ asset("assets/vendor_plugins/DataTables-1.10.15/media/assets/js/jquery.dataTables.min.js") }}"></script>
<script src="{{asset("js/jquery.mask.js")}}"></script>

<script src="{{ asset('js/pages/dashboard.js') }}"></script>
<script src="{{ asset('js/pages/dashboard-chart.js') }}"></script>
<script src="{{ asset('js/pages/advanced-form-element.js') }}"></script>
<script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>
<script>
    /**
     * Abrir modal de dividir lucro no admin
     */


    $(document).ready(function () {
        @if (isset($errors) && count($errors) > 0)
        @foreach ($errors->all() as $error)
        swal("Erro!", "{{ $error }}", "error");
        @endforeach
        @endif

        @if(session()->has('status') && session()->get('status') == 200)
        swal("Sucesso!", "{{ session()->get('msg') }}", "success");
        @endif
        @if(session()->has('status') && session()->get('status') == 400)
        swal("Erro!", "{{ session()->get('msg') }}", "error");
        @endif

        $('.valor').maskMoney({
            prefix: '',
            allowNegative: false,
            thousands: '',
            decimal: '.',
            affixesStay: false
        });

        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0.00", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
            translation: {
                'r': {
                    pattern: /[\/]/,
                    fallback: '/'
                },
                placeholder: "__/__/____"
            }
        });
    })
    ;
</script>
@yield('page-js')
</body>
</html>

