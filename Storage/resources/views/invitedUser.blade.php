<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('css/img/NSystemSolutions.png') }}">

    <title>Build Management- Lockscreen </title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-extend.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/master_style.css') }}">

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">

    <div class="lockscreen-box-body">

        <div class="lockscreen-logo">
            <a href="#"><b>Building</b>Management</a>
        </div>

        <!-- START LOCK SCREEN ITEM -->
        <div class="lockscreen-item">
            <!-- lockscreen image -->
            <div class="lockscreen-image">
                <img src="{{ asset('images/user-info.jpg') }}" alt="User Image">
            </div>
            <!-- /.lockscreen-image -->

            <!-- User name -->
            <div class="lockscreen-name margin-top-20"><h3>
                    Company: {{ \App\Companie::where('id_companie', '=', $data['companie_invites_id_companie'])->first()->companie_name }}</h3></div>
            <div class="">
                <h6>Fill in the fields below to register, and be able to use our tools :) </h6>
            </div>
            <!-- lockscreen credentials (contains the form) -->
            <form method="POST" action="{{ route('invited.user') }}" class="form-element">
                @csrf
                <input name="id_companie" hidden value="{{ $data['companie_invites_id_companie'] }}">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" required name="first_name" value="{{ old('first_name') }}" placeholder="First Name">
                    <span class="ion ion-person form-control-feedback "></span>
                    @error('first_name')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" required name="last_name" value="{{ old('last_name') }}" placeholder="Last name">
                    <span class="ion ion-person form-control-feedback "></span>
                    @error('last_name')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input type="email" readonly name="email" class="form-control" required value="{{ $data['companie_invites_email'] }}" placeholder="E-mail">
                    <span class="ion ion-email form-control-feedback "></span>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" required  placeholder="Password">
                    <span class="ion ion-locked form-control-feedback "></span>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password_confirmation" class="form-control" required placeholder="Password Confirmation">
                    <span class="ion ion-log-in form-control-feedback "></span>
                </div>
                <div class="row">
                    <!-- <div class="col-12">
                        <div class="checkbox">
                            <input type="checkbox" id="basic_checkbox_1" >
                            <label for="basic_checkbox_1">Eu aceito os <a href="#"><b>Termos</b></a></label>
                        </div>
                    </div>
                    /.col -->
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-info btn-block margin-top-10">Sign Up</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <!-- /.lockscreen credentials -->

        </div>
        <!-- /.lockscreen-item -->
    </div>
</div>
<!-- /.center -->

<!-- jQuery 3 -->
<script src="{{ asset('assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

<!-- popper -->
<script src=" {{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>

<!-- Bootstrap 4.0-->
<script src=" {{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        @if(session()->has('status') && session()->get('status') == 200)
        swal("Sucesso!", "{{ session()->get('msg') }}", "success");
        @endif
        @if(session()->has('status') && session()->get('status') == 400)
        swal("Erro!", "{{ session()->get('msg') }}", "error");
        @endif
    });
</script>
</body>
</html>
