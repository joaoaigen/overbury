<!DOCTYPE html>
<html>
<head>
    <title>NSystem Solutions</title>
</head>
<body>
<h1>{{ $details['title'] }}</h1>
<b><i><h2>{{ $details['body'] }}</h2></i></b>

<table style="width:100%; border: black; cellspacing:0; cellpadding: 0; border-bottom-width: 1px" class="wp-block-table action" >
    <tbody>
    <tr>
        <th align="center">NAME</th>
        <th align="center">CURRENT QUANTITY</th>
    </tr>
    <tr>
        <td align="center" style="font-size: 30px">{{ $details['name'] }}</td>
        <td align="center" style="font-size: 30px">{{ $details['quantidade'] }}</td>
    </tr>
    </tbody>
</table>

<p>Thank you</p>
</body>
</html>
