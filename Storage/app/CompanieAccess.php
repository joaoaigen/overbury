<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanieAccess extends Model
{
    protected $fillable = [
        'id_companie', 'access_routes',
    ];

    protected $primaryKey = 'id_companie_access';

    protected $table = 'companie_access';

    public function companie(){
        $this->hasMany('App\Companie', 'id_companie', 'id_companie');
    }

    public function allData(){
        return parent::all();
    }

    public function newData(array $data = []){

        return parent::create($data);
    }

    public function updateData(array $data = [], $id){

        return parent::where('id_companie_access', $id)->update($data);
    }

    public function showData($id){

        return parent::find($id);
    }

    public function showDataUsers($id){
        return parent::find($id)->users; //mostrar todos os usuarios
    }

    public function showDataCompanie($id){
        return parent::find($id)->companie; //mostrar todos os acessos da empresa
    }
}
