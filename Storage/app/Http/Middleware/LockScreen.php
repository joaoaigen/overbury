<?php

namespace App\Http\Middleware;

use App\Companie;
use Closure;
use Illuminate\Support\Facades\Auth;

class LockScreen
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->getRequestUri() == '/send-invitation')
            return $next($request);

        if ($request->getRequestUri() == '/logout')
            return $next($request);

        if (Auth::user()) {

            $result = new Companie();
            $companie = $result->showData(Auth::user()->id_companie);

            if ($companie->companie_database)
                return $next($request);
            else
                return redirect('/lockscreen');
        } else {

            return $next($request);
        }
    }
}
