<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DeveloperAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->nivel_master == 'Developer')
            return $next($request);
        else
            return redirect('/home')->with('status', 400)->with('msg', "You dont have permission to access");
    }
}
