<?php

namespace App\Http\Controllers;

use App\Routes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoutesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $class = new Routes();

        return view('admin.route.index')->with('routes', $class->allData());
    }

    /**
     * Store a newly created resource in companie.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $routes = new Routes();

        return $routes->newData($request->only('name_exhibition', 'name'));

        //return back()->with('status', 200)->with('msg', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        if ($request->req_ajax){
            $routes = new Routes();
            $routes = $routes->showData($request->id_routes);

            return response()->json([
                'status' => 200,
                'name' => $routes->name,
                'name_exhibition' => $routes->name_exhibition,
            ]);
        }
    }

    /**
     * Update the specified resource in companie.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $routes = new Routes();
        $routes = $routes->updateData($request->only(['name']), $request->only(['name_exhibition']));

        return back()->with('status', 200)->with('msg', 'Update successfully!');
    }

    /**
     * Remove the specified resource from companie.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
