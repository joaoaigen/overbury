<?php

namespace App\Http\Controllers;

use App\Companie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $class = new Companie();

        return view('companie.index')->with('companie', $class->allData());
    }

    /**
     * Store a newly created resource in companie.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $companie = new Companie();

        return $companie->newData($request->only('companie_name', 'companie_nickname', 'companie_database', 'companie_email'));

        //return back()->with('status', 200)->with('msg', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if ($request->req_ajax){
            $companie = new Companie();
            $companie = $companie->showData($request->id_companie);

            return response()->json([
                'status' => 200,
                'companie_database' => $companie->companie_database,
            ]);
        }
    }

    /**
     * Update the specified resource in companie.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $companie = new Companie();
        $companie = $companie->updateData($request->only(['companie_database']), $request->only(['id_companie']));

        return back()->with('status', 200)->with('msg', 'Update successfully!');
    }

    /**
     * Remove the specified resource from companie.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
