<?php

namespace App\Http\Controllers;

use App\HiredProducts;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HiredProductsController extends Controller
{
    public function index()
    {
        $this->database(Auth::user()->id_companie);

        $hired = new HiredProducts();

        return view('hiredProducts.index')->with('hiredProd', $hired->allData());
    }

    public function store(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        request()->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2000000',
        ]);

        if (isset($request->image)){
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('images/products'), $imageName);

            $request->merge(['photo' => $imageName]);
        }

        $hired = new HiredProducts();

        $hired->newData($request->only('product_name','enterprise_name', 'hired_date', 'photo'));

        return back()->with('status', 200)->with('msg', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $hired = new HiredProducts();
        $hired = $hired->showData($request->id_hired_products);

        return response()->json([
            'status' => 200,
            'enterprise_name' => $hired->enterprise_name,
            'hired_date' => $hired->hired_date,
            'photo' => $hired->photo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $hired = new HiredProducts();
        $hired->updateData($request->only('enterprise_name'), $request->only('id_hired_products'));

        return back()->with('status', 200)->with('msg', 'Updated successfully!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->database(Auth::user()->id_companie);
        $storage = new HiredProducts();
        $result = $storage->deleteProducts($request->id_hired_products);

        if ($result == 1){
            return back()->with('status', 200)->with('msg', 'Deleted successfully!');
        }else {
            return back()->with('status', 400)->with('msg', 'Error with this action!');
        }
    }
}
