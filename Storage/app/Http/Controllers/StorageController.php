<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use App\Storage;
use Illuminate\Support\Facades\Auth;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $class = new Storage();

        return view('storage.index')->with('storage', $class->allData());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $storage = new Storage();
        $storage->newData($request->only('quantity_current', 'id_product'));

        return back()->with('status', 200)->with('msg', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        if ($request->req_ajax){
            $storage = new Storage();
            $storage = $storage->showData($request->id_storage);

            $products = new Products();
            $products = $products->showData($storage->id_product);

            return response()->json([
                'status' => 200,
                'quantity_current' => $storage->quantity_current,
                'products_name' => $products->products_name,
                'alert_quantity' => $storage->alert_quantity

            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $storage = new Storage();
        $result = $storage->updateData($request->only(['quantity_current', 'alert_quantity']), $request->only(['id_storage']));

        if ($request->quantity_current < $request->alert_quantity){
            $details = [
                'title' => 'Mail from NSystem Solutions',
                'body' => 'O seguinte produto esta acabando: ',
                'name' => $request->products_name,
                'quantidade' => $request->quantity_current
            ];
            $storage->outOfStockNotification('newemo14@gmail.com', $details);
        }

        return back()->with('status', 200)->with('msg', 'Update successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->database(Auth::user()->id_companie);
        $storage = new Storage();
        $result = $storage->deleteStorage($request->id_storage);

        if ($result == 1){
            return back()->with('status', 200)->with('msg', 'Deleted successfully!');
        }else {
            return back()->with('status', 400)->with('msg', 'Error with this action!');
        }
    }

    public function NotificationStorage(){
        $this->database(Auth::user()->id_companie);
        $storage = new \App\Storage();

        $data = $storage->outOfStockNotification();

        dd($data);
    }
}
