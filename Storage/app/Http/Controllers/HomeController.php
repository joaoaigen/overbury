<?php

namespace App\Http\Controllers;

use App\Keys;
use App\Services\GuzzleRequests;
use Illuminate\Http\Request;
use App\Storage;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $storage = new Storage();
        $storage = $storage->allData();

        return view('home')->with('storage', $storage);
    }

}
