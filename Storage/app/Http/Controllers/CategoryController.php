<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->database(Auth::user()->id_companie);

        $category = new Category();
        $category = $category->allData();

        return view('category.index')->with('category', $category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $category = new Category();
        $category = $category->newData($request->only('category_name'));

        return back()->with('status', 200)->with('msg', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $category = new Category();
        $category = $category->showData($request->id_category);

        return response()->json([
            'status' => 200,
            'category_name' => $category->category_name
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $category = new Category();
        $category = $category->updateData($request->only('category_name'), $request->only('id_category'));

        return back()->with('status', 200)->with('msg', 'Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->database(Auth::user()->id_companie);

        $storage = new Category();
        $result = $storage->deleteCategory($request->id_category);

        if ($result == 1){
            return back()->with('status', 200)->with('msg', 'Deleted successfully!');
        }else {
            return back()->with('status', 400)->with('msg', 'Error with this action!');
        }
    }
}
