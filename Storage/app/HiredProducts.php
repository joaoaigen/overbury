<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HiredProducts extends Model
{
    protected $fillable = [
        'product_name','enterprise_name', 'hired_date', 'photo', 'created_at', 'updated_at', 'deleted_at'
    ];


    protected $primaryKey = 'id_products';

    protected $table = 'hired_products';

    public function allData(){
        return parent::all();
    }

    public function newData(array $data = []){

        return parent::create($data);
    }

    public function updateData(array $data = [], $id){

        return parent::where('id_hired_products', $id)->update($data);
    }

    public function showData($id){

        return parent::find($id);
    }

    public function deleteProducts($id){
        return parent::where('id_hired_products', $id)->delete();
    }
}
