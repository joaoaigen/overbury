<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Products extends Model
{
    protected $fillable = [
        'products_name', 'products_description', 'id_category', 'photo_name', 'created_at', 'updated_at', 'deleted_at'
    ];


    protected $primaryKey = 'id_products';

    protected $table = 'products';

    public function storage()
    {
        return $this->hasOne('App\Storage', 'id_product', 'id_products');
    }

    public function category()
    {
        return $this->hasOne('App\Category', 'id_category', 'id_category');
    }

    public function allData(){
        return parent::all();
    }

    public function newData(array $data = []){

        return parent::create($data);
    }

    public function updateData(array $data = [], $id){

        return parent::where('id_products', $id)->update($data);
    }

    public function showData($id){

        return parent::find($id);
    }

    public function deleteProducts($id){
        return parent::where('id_products', $id)->delete();
    }

}
