<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanieInvites extends Model
{
    protected $fillable = [
        'companie_invites_email', 'companie_invites_id_companie', 'companie_invites_token'
    ];

    protected $primaryKey = 'id_companie_invites';

    protected $table = 'companie_invites';


    public function companieAccess(){
        $this->hasOne('App\Companie', 'id_companie', 'id_companie_invites');
    }

    public function allData(){
        return parent::all();
    }

    public function newData(array $data = []){

        return parent::create($data);
    }

    public function updateData(array $data = [], $id){

        return parent::where('id_companie_invites', $id)->update($data);
    }

    public function showData($id){

        return parent::find($id);
    }

    public function showDataCompanie($id){
        return parent::find($id)->companieAccess; //mostrar todos os acessos da empresa
    }
}
