<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Parent_;

class Storage extends Model
{
    protected $fillable = [
        'id_product', 'alert_quantity', 'quantity_current', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $primaryKey = 'id_storage';

    protected $table = 'storage';

    public function products()
    {
        return $this->hasOne('App\Products', 'id_products', 'id_product');
    }

    public function allData()
    {
        $all = DB::table('storage')
            ->join('products', 'storage.id_product', '=', 'products.id_products')
            ->join('category', 'products.id_category', '=', 'category.id_category')
            ->get();

        return $all;
    }

    public function newData($data = array())
    {
        return parent::create($data);
    }

    public function updateData(array $data = [], $id = 0)
    {
        $store = parent::where('id_storage', '=', $id)->update($data);

        return $store;
    }

    public function showData($id)
    {
        return parent::find($id);
    }

    public function deleteStorage($id){
        return parent::where('id_storage', $id)->delete();
    }

    public function outOfStockNotification($email, $details)
    {
        \Mail::to($email)->send(new \App\Mail\Storage($details));
    }
}
