<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companie extends Model
{
    protected $fillable = [
        'companie_name', 'companie_nickname', 'companie_database', 'companie_email', 'companie_url_logo'
    ];

    protected $primaryKey = 'id_companie';

    protected $table = 'companie';

    public function users(){
        $this->hasMany('App\Users', 'id_companie', 'id_companie');
    }

    public function companieAccess(){
        $this->hasOne('App\CompanieAccess', 'id_companie', 'id_companie');
    }

    public function companieUsersAccess(){
        $this->hasOne('App\CompanieUsersAccess', 'id_companie', 'id_companie');
    }

    public function allData(){
        return parent::all();
    }

    public function newData(array $data = []){

        return parent::create($data);
    }

    public function updateData(array $data = [], $id){

        return parent::where('id_companie', $id)->update($data);
    }

    public function showData($id){

        return parent::find($id);
    }

    public function showDataUsers($id){
        return parent::find($id)->users; //mostrar todos os usuarios
    }

    public function showDataCompanieAccess($id){
        return parent::find($id)->companieAccess; //mostrar todos os acessos da empresa
    }

    public function showDataCompanieUsersAccess($id){
        return parent::find($id)->companieUsersAccess; //mostrar dos os acessos dos usuarios da empresa
    }
}
