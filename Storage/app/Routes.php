<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routes extends Model
{
    protected $fillable = [
        'name_exhibition', 'name',
    ];

    protected $primaryKey = 'id_routes';

    protected $table = 'routes';

    public function allData(){
        return parent::all();
    }

    public function newData(array $data = []){

        return parent::create($data);
    }

    public function updateData(array $data = [], $id){

        return parent::where('id_routes', $id)->update($data);
    }

    public function showData($id){

        return parent::find($id);
    }

    public function showDataUsers($id){
        return parent::find($id)->users; //mostrar todos os usuarios
    }

    public function showDataCompanieAccess($id){
        return parent::find($id)->companieAccess; //mostrar todos os acessos da empresa
    }

    public function showDataCompanieUsersAccess($id){
        return parent::find($id)->companieUsersAccess; //mostrar dos os acessos dos usuarios da empresa
    }
}
