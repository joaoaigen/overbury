<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password', 'id_companie', 'id_users_access', 'nivel'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function companie(){
        $this->hasOne('App\Companie', 'id_companie', 'id_companie');
    }

    function userData($id){
        return parent::where('id', $id)->first();
    }

    function userCompanie($id){
        return parent::where('id', $id)->first()->companie;
    }

    function updateUser($data){
        return parent::where('id', $data['id'])->updated($data);
    }

    function createUser($data){


        User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'id_companie' => $data['id_companie'],
        ]);



        return redirect('/home')->with('status', 200)->with('msg', 'User created successfully.');
    }
}
