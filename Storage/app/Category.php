<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'category_name', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $primaryKey = 'id_category';

    protected $table = 'category';

    public $timestamps = true;

    public function allData(){
        return parent::all();
    }

    public function newData(array $data = []){

        return parent::create($data);
    }

    public function updateData(array $data = [], $id){

        return parent::where('id_category', $id)->update($data);
    }

    public function showData($id){

        return parent::find($id);
    }

    public function deleteCategory($id){
        return parent::where('id_category', $id)->delete();
    }
}
