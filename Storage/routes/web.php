<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('invited-user', function (){
    $data = \Illuminate\Http\Request::capture()->request->all();

    $invitedUser = \App\CompanieInvites::where('companie_invites_token', $data['token'])->first();


    return view('invitedUser')->with('data', $invitedUser);
});

Route::post('invited-user', function (){
    $data = \Illuminate\Http\Request::capture()->request->all();
    $user = new \App\User();

    $user->createUser($data);

    return redirect('/home');
})->name('invited.user');

Route::get('/', function () {
    return redirect('/home');
});

Route::get('lockscreen', function () {
    return view('lockscreen');
});

Route::group(['middleware' => ['web', 'lockscreen']], function () {

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/logout', function () {
        \Illuminate\Support\Facades\Auth::logout();

        return redirect('/login');
    });

    Route::group(['prefix' => 'storage'], function () {
        Route::get('/', 'StorageController@index')->name('storage');

        Route::post('store', 'StorageController@store')->name('storage.store');

        Route::post('update', 'StorageController@update')->name('storage.update');

        Route::get('show', 'StorageController@show')->name('storage.show');

        Route::get('delete', 'StorageController@destroy')->name('storage.delete');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'CategoryController@index')->name('category');

        Route::post('update', 'CategoryController@update')->name('category.update');

        Route::post('store', 'CategoryController@store')->name('category.store');

        Route::get('show', 'CategoryController@show')->name('category.show');

        Route::get('delete', 'CategoryController@destroy')->name('category.delete');
    });

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'ProductsController@index')->name('products');

        Route::post('update', 'ProductsController@update')->name('products.update');

        Route::post('store', 'ProductsController@store')->name('products.store');

        Route::get('show', 'ProductsController@show')->name('products.show');

        Route::get('delete', 'ProductsController@destroy')->name('products.delete');
    });

    Route::group(['prefix' => 'hired-products'], function () {
        Route::get('/', 'HiredProductsController@index')->name('hired');

        Route::post('update', 'HiredProductsController@update')->name('hired.update');

        Route::post('store', 'HiredProductsController@store')->name('hired.store');

        Route::get('show', 'HiredProductsController@show')->name('hired.show');

        Route::get('delete', 'HiredProductsController@destroy')->name('hired.delete');
    });

    Route::group(['middleware' => ['developer']], function () {

        Route::group(['prefix' => 'companie'], function () {
            Route::get('/', 'CompanieController@index')->name('companie');

            Route::post('update', 'CompanieController@update')->name('companie.update');

            Route::post('store', 'CompanieController@store')->name('companie.store');

            Route::get('show', 'CompanieController@show')->name('companie.show');
        });

        Route::group(['prefix' => 'routes'], function () {
            Route::get('/', 'RoutesController@index')->name('routes');

            Route::post('update', 'RoutesController@update')->name('routes.update');

            Route::post('store', 'RoutesController@store')->name('routes.store');

            Route::get('show', 'RoutesController@show')->name('routes.show');
        });

    });


    // funcoes abaixo -------------------------------------------------------------------------------

    Route::post('send-invitation', function () {
        $email = \Illuminate\Http\Request::capture()->request->all();

        $user = \App\User::where('email', '=', $email['email'])->first();

        if (empty($user)){
            $companie = new \App\Companie();
            $invite = new \App\CompanieInvites();

            $companie = $companie->showData(\Illuminate\Support\Facades\Auth::user()->id_companie); //pega a compania do usuario registrado ja, para que o proximo seja registrado na mesma

            $link = 'http://134.209.125.155/invited-user/?token='.$email['_token'];

            $arr = [
                'companie_invites_email' => $email['email'],
                'companie_invites_id_companie' => \Illuminate\Support\Facades\Auth::user()->id_companie,
                'companie_invites_token' => $email['_token']
            ];

            $invite = $invite->newData($arr);

            $details = [
                'title' => 'Mail from NSystem Solutions',
                'body' => 'Hello! You have been invited to register on the NSystem solutions platform, a management system for construction. You will be registered with the company:',
                'companie_name' =>  $link

            ];

            \Mail::to($email['email'])->send(new \App\Mail\InviteUser($details));

            return back()->with('status', 200)->with('msg', 'Email successfully sent!');
        }else{
            return back()->with('status', 400)->with('msg', 'User already exists, or has already been invited.');
        }


    })->name('send-invitation');

    Route::get('storage-notification', 'StorageController@NotificationStorage');
});
